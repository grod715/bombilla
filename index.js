// Variables
var express = require('express')
var app = express()
var server = require('http').createServer(app)
var  io = require('socket.io').listen(server)
var port = process.env.PORT || 3000
server.listen(port);

console.log("Corriendo Servidor en el puerto " +port); 
console.log("dominio: " + server.domain);
app.use(express.static(__dirname + "/"));

app.get('/', function(req, res){
	res.sendFile(__dirname + '/index.html');
});
var estado=false;
io.on('connection', function(sc){
	sc.emit('estado',estado);
	console.log("un usuario se ha conectado");
	
	sc.on('cambio',function(x){
    	io.emit('estado',estado=!estado);
		console.log("estado "+estado)
	})

	sc.on('disconnect',function(sc){
		console.log("un usuario se ha desconectado")
	})
});

